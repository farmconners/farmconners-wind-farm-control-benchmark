# FarmConners Wind Farm Control Benchmark

Welcome to the repository! Please find the benchmark documentation [here](https://farmconners.readthedocs.io/).

![flyer](docs/source/images/benchmark_flyer.png)

<img align="left" src="docs/source/images/flag_yellow_high.jpg"  width="100">

The FarmConners project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No. 857844.
