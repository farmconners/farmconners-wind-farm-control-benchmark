<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Quantities of Interest &mdash; FarmConners Wind Farm Control Benchmark  documentation</title>
      <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
      <link rel="stylesheet" href="_static/css/custom.css" type="text/css" />
      <link rel="stylesheet" href="_static/css/custom2.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
        <script src="_static/jquery.js"></script>
        <script src="_static/underscore.js"></script>
        <script src="_static/doctools.js"></script>
        <script defer="defer" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="_static/js/theme.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="General Taxonomy and Nomenclature" href="taxonomy_and_nomenclature.html" />
    <link rel="prev" title="Test Cases" href="test_cases.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
            <a href="index.html" class="icon icon-home"> FarmConners Wind Farm Control Benchmark
          </a>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <ul class="current">
<li class="toctree-l1"><a class="reference internal" href="provided_data_sets.html">Provided Data Sets</a></li>
<li class="toctree-l1"><a class="reference internal" href="test_cases.html">Test Cases</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Quantities of Interest</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#power-gain">Power gain</a></li>
<li class="toctree-l2"><a class="reference internal" href="#wake-loss-reduction">Wake loss reduction</a></li>
<li class="toctree-l2"><a class="reference internal" href="#reduction-in-wake-added-turbulence-intensity">Reduction in wake-added Turbulence Intensity</a></li>
<li class="toctree-l2"><a class="reference internal" href="#load-alleviation">Load Alleviation</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="taxonomy_and_nomenclature.html">General Taxonomy and Nomenclature</a></li>
<li class="toctree-l1"><a class="reference internal" href="frequently_asked_questions.html">FAQ</a></li>
<li class="toctree-l1"><a class="reference internal" href="contact_us.html">Contact Us</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">FarmConners Wind Farm Control Benchmark</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="index.html" class="icon icon-home"></a> &raquo;</li>
      <li>Quantities of Interest</li>
      <li class="wy-breadcrumbs-aside">
            <a href="_sources/quantities_of_interest.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="quantities-of-interest">
<span id="id1"></span><h1>Quantities of Interest<a class="headerlink" href="#quantities-of-interest" title="Permalink to this headline"></a></h1>
<p>The performance of the WFC model participating in the benchmark is to be evaluated based on wind speed, power and turbine response criteria. It should be noted that the models should be run according to their capabilities. Due to the control-oriented character of the benchmark, mainly steady-state and quasi-dynamic low-cost models are expected to provide solutions. However, depending on the model fidelity, the statistical quantities, <em>i.e.</em> mean, median, standard deviation, etc. from time series of 3-dimensional flow fields can also be extracted.</p>
<p>The resulting quantities of interest for the benchmark are listed below. In order to provide a generic overview, the metrics to be used for validation and comparison are non-dimensionalised. The statistical metrics to be used for these quantities are the median and quartiles of both the observation and simulation ensemble. The uploaded results will be processed by the benchmark organisers to diagnose quantities of interest with the publicly released notebooks on the <a class="reference external" href="https://gitlab.windenergy.dtu.dk/farmconners/farmconners-wind-farm-control-benchmark">GitLab repository</a>.</p>
<p>For the final analysis of the quantities of interest, the flow and turbine response behaviour under WFC are to be compared with the normal operation conditions. Therefore, for a fair comparison, it is of utmost importance that the inflow for both WFC and normal operational cases are statistically similar.</p>
<section id="power-gain">
<h2>Power gain<a class="headerlink" href="#power-gain" title="Permalink to this headline"></a></h2>
<p>Commonly referred in literature, the potential power gain under WFC should be assessed with respect to the normal operation for the whole wind farm in question.</p>
<div class="math notranslate nohighlight">
\[\Delta P = \frac{\left({\sum_{i=1}^{n}P_i}\right)_{Op.=WFC}}{\left({\sum_{i=1}^{n}P_i}\right)_{Op.=Normal}} -1\]</div>
<p><span class="math notranslate nohighlight">\(n\)</span> is the total number of turbines considered in the test case,  <span class="math notranslate nohighlight">\(P\)</span> is the power produced by turbine <span class="math notranslate nohighlight">\(i\)</span>, and <span class="math notranslate nohighlight">\(Op.=WFC\)</span> and <span class="math notranslate nohighlight">\(Op.=Normal\)</span> correspond to operation under WFC and normal operation, respectively.</p>
</section>
<section id="wake-loss-reduction">
<h2>Wake loss reduction<a class="headerlink" href="#wake-loss-reduction" title="Permalink to this headline"></a></h2>
<p>Although highly correlated to power gain, the potential reduction in the wake losses under WFC is another quantity of interest for the WFC technology stakeholders. This is mainly due to the more scalable character of the wake loss reduction from simpler 2-turbine configurations to more complex wind farm layouts. It also has the benefit to be able to partially mitigate wind-to-power conversion uncertainties for certain WFC models.</p>
<div class="math notranslate nohighlight">
\[\Delta u = \frac{{\sum_{j=1}^{n-1}(U_{up}-U_{1+j}})_{Op.=Normal}-{\sum_{j=1}^{n-1}(U_{up}-U_{1+j})}_{Op.=WFC}}{U_{up}}\]</div>
<p><span class="math notranslate nohighlight">\(\Delta u\)</span> is the non-dimensionalised wake loss at the wind farm level, <span class="math notranslate nohighlight">\(U\)</span> is either the hub height or rotor effective wind speed, <span class="math notranslate nohighlight">\(REWS\)</span>, that represents the spatially averaged wind speed over the rotor; at the upstream, <span class="math notranslate nohighlight">\(U_{up}\)</span>, and downstream turbine(s), <span class="math notranslate nohighlight">\(U_{1+j}\)</span>.</p>
</section>
<section id="reduction-in-wake-added-turbulence-intensity">
<h2>Reduction in wake-added Turbulence Intensity<a class="headerlink" href="#reduction-in-wake-added-turbulence-intensity" title="Permalink to this headline"></a></h2>
<p>The added TI is an important feature for the wind farm flow, which increases both the variability in power production and structural loads of the downstream turbine(s). Accordingly, a potential reduction in wake-added TI  with WFC techniques is a quantity of interest.</p>
<div class="math notranslate nohighlight">
\[\Delta TI_i = \left(\frac{\sigma_{U_i}}{\overline{U_i}}\right)_{Op.=Normal} -  \left(\frac{\sigma _{U_i}}{\overline{U_i}}\right)_{Op.=WFC} \ \ \ \text{where} \ \ \ i = 2, n\]</div>
</section>
<section id="load-alleviation">
<h2>Load Alleviation<a class="headerlink" href="#load-alleviation" title="Permalink to this headline"></a></h2>
<p>One of the important use cases for WFC is the mitigation of structural loads on the turbines. In order to evaluate the model performance on potential load reduction under WFC in the defined test cases, the validation and the comparison of the models will be based on the normalised Damage Equivalent Loads (DEL) of the flapwise root bending moment <span class="math notranslate nohighlight">\(FlapM\)</span>, total shaft bending moment <span class="math notranslate nohighlight">\(ShaftM\)</span> and total tower bottom bending moment <span class="math notranslate nohighlight">\(TBBM\)</span>. Similar to power gain and wake loss reduction metrics, the reduction in loads is assessed in comparison to the normal operation. Note that the dataset to assess this quantity of interest is limited to <a class="reference internal" href="provided_data_sets.html#sytheticdata"><span class="std std-ref">Synthetic data set - Large Eddy Simulations</span></a> and <a class="reference internal" href="provided_data_sets.html#windtunnelexp"><span class="std std-ref">CL-Windcon Wind Tunnel Experiments</span></a>, as the available field measurements are deemed to be inconclusive for such an analysis at this stage.</p>
<div class="math notranslate nohighlight">
\[\begin{split}\Delta DEL_{\substack{FlapM \\ ShaftM \\ TBBM}} = 1 - \left(\frac{DEL_{\substack{FlapM \\ ShaftM \\ TBBM}_{{Op.=WFC}}}}{DEL_{\substack{FlapM \\ ShaftM \\ TBBM}_{{Op.=Normal}}}}\right)_{i} \\
\ \ \ \ \ \text{i = 1, n}\end{split}\]</div>
</section>
</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="test_cases.html" class="btn btn-neutral float-left" title="Test Cases" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="taxonomy_and_nomenclature.html" class="btn btn-neutral float-right" title="General Taxonomy and Nomenclature" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2020, Tuhfe Göçmen.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>