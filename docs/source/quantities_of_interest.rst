.. _quantities_of_interest:

Quantities of Interest
=======================

The performance of the WFC model participating in the benchmark is to be evaluated based on wind speed, power and turbine response criteria. It should be noted that the models should be run according to their capabilities. Due to the control-oriented character of the benchmark, mainly steady-state and quasi-dynamic low-cost models are expected to provide solutions. However, depending on the model fidelity, the statistical quantities, *i.e.* mean, median, standard deviation, etc. from time series of 3-dimensional flow fields can also be extracted.

The resulting quantities of interest for the benchmark are listed below. In order to provide a generic overview, the metrics to be used for validation and comparison are non-dimensionalised. The statistical metrics to be used for these quantities are the median and quartiles of both the observation and simulation ensemble. The uploaded results will be processed by the benchmark organisers to diagnose quantities of interest with the publicly released notebooks on the `GitLab repository <https://gitlab.windenergy.dtu.dk/farmconners/farmconners-wind-farm-control-benchmark>`_.

For the final analysis of the quantities of interest, the flow and turbine response behaviour under WFC are to be compared with the normal operation conditions. Therefore, for a fair comparison, it is of utmost importance that the inflow for both WFC and normal operational cases are statistically similar. 


Power gain
--------------------------------------
Commonly referred in literature, the potential power gain under WFC should be assessed with respect to the normal operation for the whole wind farm in question.

.. math::
	\Delta P = \frac{\left({\sum_{i=1}^{n}P_i}\right)_{Op.=WFC}}{\left({\sum_{i=1}^{n}P_i}\right)_{Op.=Normal}} -1

:math:`n` is the total number of turbines considered in the test case,  :math:`P` is the power produced by turbine :math:`i`, and :math:`Op.=WFC` and :math:`Op.=Normal` correspond to operation under WFC and normal operation, respectively.



Wake loss reduction
---------------------------
Although highly correlated to power gain, the potential reduction in the wake losses under WFC is another quantity of interest for the WFC technology stakeholders. This is mainly due to the more scalable character of the wake loss reduction from simpler 2-turbine configurations to more complex wind farm layouts. It also has the benefit to be able to partially mitigate wind-to-power conversion uncertainties for certain WFC models.

.. math::
	\Delta u = \frac{{\sum_{j=1}^{n-1}(U_{up}-U_{1+j}})_{Op.=Normal}-{\sum_{j=1}^{n-1}(U_{up}-U_{1+j})}_{Op.=WFC}}{U_{up}} 

:math:`\Delta u` is the non-dimensionalised wake loss at the wind farm level, :math:`U` is either the hub height or rotor effective wind speed, :math:`REWS`, that represents the spatially averaged wind speed over the rotor; at the upstream, :math:`U_{up}`, and downstream turbine(s), :math:`U_{1+j}`. 


Reduction in wake-added Turbulence Intensity 
---------------------------------------------------
The added TI is an important feature for the wind farm flow, which increases both the variability in power production and structural loads of the downstream turbine(s). Accordingly, a potential reduction in wake-added TI  with WFC techniques is a quantity of interest.

.. math::
	\Delta TI_i = \left(\frac{\sigma_{U_i}}{\overline{U_i}}\right)_{Op.=Normal} -  \left(\frac{\sigma _{U_i}}{\overline{U_i}}\right)_{Op.=WFC} \ \ \ \text{where} \ \ \ i = 2, n



Load Alleviation
---------------------------
One of the important use cases for WFC is the mitigation of structural loads on the turbines. In order to evaluate the model performance on potential load reduction under WFC in the defined test cases, the validation and the comparison of the models will be based on the normalised Damage Equivalent Loads (DEL) of the flapwise root bending moment :math:`FlapM`, total shaft bending moment :math:`ShaftM` and total tower bottom bending moment :math:`TBBM`. Similar to power gain and wake loss reduction metrics, the reduction in loads is assessed in comparison to the normal operation. Note that the dataset to assess this quantity of interest is limited to :ref:`Sytheticdata` and :ref:`WindTunnelExp`, as the available field measurements are deemed to be inconclusive for such an analysis at this stage.

.. math::
	\Delta DEL_{\substack{FlapM \\ ShaftM \\ TBBM}} = 1 - \left(\frac{DEL_{\substack{FlapM \\ ShaftM \\ TBBM}_{{Op.=WFC}}}}{DEL_{\substack{FlapM \\ ShaftM \\ TBBM}_{{Op.=Normal}}}}\right)_{i} \\
	\ \ \ \ \ \text{i = 1, n}
