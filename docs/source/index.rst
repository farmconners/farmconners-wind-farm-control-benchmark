.. FarmConners Wind Farm Control Benchmark documentation master file, created by
   sphinx-quickstart on Fri May 15 11:07:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. _index:

Welcome to FarmConners Wind Farm Control Benchmark for code comparison!
================================================================================

**The benchmark is closed now - Thanks to everyone who participated! The first part of the results are now published in WES journal** `here <https://wes.copernicus.org/preprints/wes-2022-5>`_. 


Careful validation of the modelling and control actions is of vital importance to build confidence in the value of coordinated wind farm control (WFC). The efficiency of flow models applied to WFC should be evaluated to provide reliable assessment of the performance of WFC.  In order to achieve that, the `FarmConners project <https://www.windfarmcontrol.info/>`_ launches a common benchmark for code comparison to demonstrate the potential benefits of WFC, such as increased power production and mitigation of loads. 

The online recording of the benchmark launch can be watched `here <https://youtu.be/rCOAW8rMfKc>`_.

The benchmark repository is located `here <https://gitlab.windenergy.dtu.dk/farmconners/farmconners-wind-farm-control-benchmark>`_.

The comparison of the WFC model results for the defined test cases will first be presented in WESC 2021 and then sent for publication in co-authorship with all the benchmark participants. All the registered participants receives a link to a personalised cloud folder to update their results.

.. image:: images/benchmark_flyer.png
  :width: 700
  
  
The benchmark builds on available data sets from previous and ongoing campaigns: synthetic data from high-fidelity simulations, measurements from wind tunnel experiments, and field data from a real wind farm, see :ref:`provided_data_sets`.

The participating WFC models are first to be calibrated or trained using normal operation periods. For the blind test, both the axial induction and wake steering control approaches are included in the dataset and to be evaluated through the designed test cases. Three main test cases are specified, addressing the impact of WFC on single full wake, single partial wake, and multiple full wake, see :ref:`test_cases`.

The WFC model outcomes will be compared during the blind test phase, through power gain and wake loss reduction as well as alleviation of wake-added turbulence intensity and structural loads. The probabilistic validation will be based on the median and quartiles of the observations and WFC model predictions of the :ref:`quantities_of_interest`.

Every benchmark participant will be involved in the final publication, where  the comparison of different tools will be performed using the defined test cases. Instructions on how to participate are provided  on :ref:`how_to_participate`.

..  figure:: images/flag_yellow_high.jpg
	:figwidth: 100
	:align: left
  
The FarmConners project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No. 857844.


Content
=======
.. toctree::
   :maxdepth: 2

   provided_data_sets.rst
   test_cases.rst
   quantities_of_interest.rst
   taxonomy_and_nomenclature.rst
   frequently_asked_questions.rst
   contact_us.rst
