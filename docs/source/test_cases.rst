.. _test_cases:

Test Cases
===============

The test cases follow an increasing complexity level, i.e. starting from easier configurations (*e.g.* 1-2 turbines with quasi-stationary flow and 
higher availability of information/data regarding the input and output features of the models). The test cases then gradually converge to several 
turbines and real atmospheric conditions with relatively limited measurements with higher uncertainty. The collected results from all the test cases 
will be further classified with respect to inflow conditions including wind speed and turbulence intensity to assess the sensitivity to incoming flow conditions. Due to limited availability in convection measurements/observations in the provided dataset, the sensitivity analysis with respect to 
atmospheric stability (and shear) is omitted from the benchmark.

.. _TC_single:

Single Full Wake under WFC
------------------------------------------
..  figure:: images/SingleWake_sketch.png
    :figwidth: 40%
    :align: right

The test case aims to investigate the quantities of interest in the single wake behind a controlled turbine, for a fully aligned 2-turbine configuration. It should be noted that for some of the available dataset, the configuration is a subset of the wind farm, *i.e.* there are additional blockage effects. The spacing between the turbines are also varied per data set. Both the axial induction and wake redirection WFC approaches will be evaluated under this test case.

Available data sets for that benchmark:
***************************************
* :ref:`Sytheticdata`:

	* TotalControl LES: Ellipsys3D Actuator line (northerly wind :code:`rot90`). 5-diameters distance between the turbines in a wind farm environment where turbines :code:`29` and :code:`32` are yawed.

	* CL-Windcon LES: subset of 3WT and 9WT scenarios.

* :ref:`WindTunnelExp`: 5-diameters spacing between the wind turbines. Axial induction is implemented by modifying the values of the reference rotor speed and torque. Wake steering WFC approach is conducted with the first machine misaligned of :math:`-40^\circ \text{ to } 40^\circ` with steps of :math:`10^\circ`.

* :ref:`FieldData`: 3.7-diameters spacing between turbine pairs SMV6-SMV5. Axial induction is implemented via down-regulated power curve. Wake steering WFC approach is conducted with :math:`13^\circ \text{ to } 15^\circ` yaw misalignment of the first turbine.

Expected model results for that benchmark:
********************************************
Here is the list of variables to upload the model results. They are to be uploaded to a personally assigned cloud folder to the registered benchmark participants, see :ref:`how_to_participate`. The participants are also required to provide a brief description of the model they have used to obtain the results, in any human-readable text format, including references to the relevant publications wherever applicable.

The variables are to be provided as time series, wherever possible. If the model used is limited to statistical properties only, at least the *median*, *mean*, *min*, *max* and *std* of the listed variables should be uploaded per (binned) time period.

* :ref:`Sytheticdata`:

	* TotalControl LES: Both for Normal operation and under WFC 
	
		* Power produced by the turbines: :code:`P_29`-:code:`P_25` and :code:`P_32`-:code:`P_28` 
		* Hub-height and/or rotor effective wind speed at the turbines: :code:`U_29`-:code:`U_25` and :code:`U_32`-:code:`U_28` and/or :code:`REWS_29`-:code:`REWS_25` and :code:`REWS_32`-:code:`REWS_28`
		* Flapwise root bending moment at the turbines: :code:`FlapM_29`-:code:`FlapM_25` and :code:`FlapM_32`-:code:`FlapM_28`
		* Total (vector sum) shaft bending moment at the turbines: :code:`ShaftM_29`-:code:`ShaftM_25` and :code:`ShaftM_32`-:code:`ShaftM_28`
		* Total (vector sum) tower bottom bending moment at the turbines: :code:`TBBM_29`-:code:`TBBM_25` and :code:`TBBM_32`-:code:`TBBM_28` 

	* CL-Windcon LES: Both for Normal operation and under WFC
	
		* Power produced by the turbines: :code:`P_T0`, :code:`P_T1` 
		* Hub-height and/or rotor effective wind speed at the turbines: :code:`U_T0`, :code:`U_T1` and/or :code:`REWS_T1`, :code:`REWS_T2`
		* Flapwise root bending moment at the turbines: :code:`FlapM_T0`, :code:`FlapM_T1` 
		* Total (vector sum) shaft bending moment at turbines: :code:`ShaftM_T0`, :code:`ShaftM_T1`
		* Total (vector sum) tower bottom bending moment at the turbines: :code:`TBBM_T0`, :code:`TBBM_T1`

* :ref:`WindTunnelExp`: Both for Normal operation and under WFC

		* Power produced by the turbines: :code:`P_WT1`, :code:`P_WT2` 
		* Longitudinal wind speed at the wake measurement locations: :code:`U_wake1`, :code:`U_wake2`, :code:`...`
		* Total (vector sum) shaft bending moment at the turbines: :code:`ShaftM_WT1`, :code:`ShaftM_WT2`
		* Total (vector sum) tower bottom bending moment at the turbines: :code:`TBBM_WT1`, :code:`TBBM_WT2`

* :ref:`FieldData`: Both for Normal operation and under WFC

		* Power produced by the turbines: :code:`P_SMV5`, :code:`P_SMV6` 
		* Hub-height and/or rotor effective wind speed at the turbines: :code:`U_SMV5`, :code:`U_SMV6` and/or :code:`REWS_SMV5`, :code:`REWS_SMV6`


.. _TC_partial:

Single Partial Wake under WFC
------------------------------------------
..  figure:: images/SinglePartialWake_sketch.png
    :figwidth: 40%
    :align: right

In this test case, the objective is to assess the added value of implementing wake steering WFC approach in a two-turbine configuration with lateral distance. Similar to the other scenarios, only the first turbine in the 2-turbine wind farm configuration is controlled. The spacings between the turbines are varied per data set.

Available data sets for that benchmark:
***************************************
* :ref:`Sytheticdata`:

	* TotalControl LES: Ellipsys3D Actuator line (westerly wind :code:`rot00`), 5-diameters axial and 2.5-diameters lateral distance between the nacelle positions where turbines :code:`5` and :code:`25` are yawed. 

* :ref:`WindTunnelExp`: 5-diameters longitudinal spacing between the wind turbines, 0.5-diameter lateral spacing. Wake steering WFC approach is conducted with the first machine misaligned of :math:`30^\circ`.

Expected model results for that benchmark:
********************************************
Here is the list of variables to upload the model results. They are to be uploaded to a personally assigned cloud folder to the registered benchmark participants, see :ref:`how_to_participate`. The participants are also required to provide a brief description of the model they have used to obtain the results, in any human-readable text format, including references to the relevant publications wherever applicable.

The variables are to be provided as time series, wherever possible. If the model used is limited to statistical properties only, at least the *median*, *mean*, *min*, *max* and *std* of the listed variables should be uploaded per (binned) time period.

* :ref:`Sytheticdata`:

	* TotalControl LES: Both for Normal operation and under WFC 
	
		* Power produced by the turbines: :code:`P_5`-:code:`P_10`-:code:`P_6` and :code:`P_25`-:code:`P_30`-:code:`P_26`
		* Hub-height and/or rotor effective wind speed at the turbines: :code:`U_10`-:code:`U_6` and :code:`U_30`-:code:`U_26` and/or :code:`REWS_10`-:code:`REWS_6` and :code:`REWS_30`-:code:`REWS_26`
		* Flapwise root bending moment at the turbines: :code:`FlapM_5`-:code:`FlapM_10`-:code:`FlapM_6` and :code:`FlapM_25`-:code:`FlapM_30`-:code:`FlapM_26`
		* Total shaft bending moment at the turbines: :code:`ShaftM_5`-:code:`ShaftM_10`-:code:`ShaftM_6` and :code:`ShaftM_25`-:code:`ShaftM_30`-:code:`ShaftM_26`
		* Total tower bottom bending moment at the turbines: :code:`TBBM_5`-:code:`TBBM_10`-:code:`TBBM_6` and :code:`TBBM_25`-:code:`TBBM_30`-:code:`TBBM_26`

	
* :ref:`WindTunnelExp`: Both for Normal operation and under WFC

		* Power produced by the turbines: :code:`P_WT1`, :code:`P_WT2` 
		* Longitudinal wind speed at the wake measurement locations: :code:`U_wake1`, :code:`U_wake2`, :code:`...`
		* Total (vector sum) shaft bending moment at the turbines: :code:`ShaftM_WT1`, :code:`ShaftM_WT2`
		* Total (vector sum) tower bottom bending moment at the turbines: :code:`TBBM_WT1`, :code:`TBBM_WT2`


.. _TC_multiple:

Multiple Wake under WFC
------------------------------------------
..  figure:: images/MultipleWake_sketch.png
    :figwidth: 40%
    :align: right

The test case aims to investigate the quantities of interest in a row of turbines. Depending on the dataset, either only the first turbine or a number of upstream turbines in the wind farm are controlled. The spacings between the turbines are not necessarily uniform, and varied per data set. Both the axial induction and wake redirection WFC approaches will be evaluated under this test case.

Available data sets for that benchmark:
***************************************
* :ref:`Sytheticdata`:

	* TotalControl LES: Ellipsys3D Actuator line (northerly wind :code:`rot90`). 5-diameters distance between the turbines in a wind farm environment where turbines :code:`29` and :code:`32` are yawed.

	* CL-Windcon LES: 3WT and 9WT scenarios

* :ref:`WindTunnelExp`: 3-turbine configuration with uniform 5-diameters spacing. Axial induction is implemented by modifying the values of the reference rotor speed and torque. Wake steering WFC approach is conducted with the first machine misaligned of :math:`30^\circ`.

* :ref:`FieldData`: 6-turbine configuration with 3.7 and 4.3-diameters spacing, *i.e.* SMV6 -- SMV1.

Expected model results for that benchmark:
********************************************
Here is the list of variables to upload the model results. They are to be uploaded to a personally assigned cloud folder to the registered benchmark participants, see :ref:`how_to_participate`. The participants are also required to provide a brief description of the model they have used to obtain the results, in any human-readable text format, including references to the relevant publications wherever applicable.

The variables are to be provided as time series, wherever possible. If the model used is limited to statistical properties only, at least the *median*, *mean*, *min*, *max* and *std* of the listed variables should be uploaded per (binned) time period.

* :ref:`Sytheticdata`:

	* TotalControl LES: Both for Normal operation and under WFC, 8-turbine rows (behind turbines :code:`29` and :code:`32`) *i.e.* :code:`i=29,25,21,17,13,9,5,1` and :code:`i=32,28,24,20,16,12,8,4`
	
		* Power produced by the turbines: :code:`P_i`
		* Hub-height and/or rotor effective wind speed at the turbines: :code:`U_i` and/or :code:`REWS_i` 
		* Flapwise root bending moment at the turbines: :code:`FlapM_i`
		* Total shaft bending moment at the turbines: :code:`ShaftM_i`
		* Total tower bottom bending moment at the turbines: :code:`TBBM_i`

	* CL-Windcon LES: Both for Normal operation and under WFC, 3WT and 9WT layouts :code:`n=3 or n=9`
	
		* Power produced by the turbines: :code:`P_Ti`, where :code:`i=0,n-1`
		* Hub-height and/or rotor effective wind speed at the turbines: :code:`U_Ti` and/or :code:`REWS_Ti`, where :code:`i=0,n-1`
		* Flapwise root bending moment at the turbines: :code:`FlapM_Ti`, where :code:`i=0,n-1`
		* Total shaft bending moment at the turbines: :code:`ShaftM_Ti`, where :code:`i=0,n-1`
		* Total tower bottom bending moment at the turbines: :code:`TBBM_Ti`, where :code:`i=0,n-1`
	
* :ref:`WindTunnelExp`: Both for Normal operation and under WFC, 3WT layout :code:`n=3` 

		* Power produced by the turbines: :code:`P_WTi`
		* Longitudinal wind speed at the wake measurement locations: :code:`U_wake1`, :code:`U_wake2`, :code:`...`
		* Total (vector sum) shaft bending moment at the turbines: :code:`ShaftM_WTi`
		* Total (vector sum) tower bottom bending moment at the turbines: :code:`TBBM_WTi`
		
* :ref:`FieldData`: Both for Normal operation and under WFC, SMV wind farm 7WT layout :code:`n=7` 

		* Power produced by the turbines: :code:`P_SMVi`, where :code:`i=1,n`
		* Hub-height and/or rotor effective wind speed at the turbines: :code:`U_SMVi` and/or :code:`REWS_SMVi`, where :code:`i=1,n`
		
		
		
