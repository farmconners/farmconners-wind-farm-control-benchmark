.. _contact_us:

Contact Us
==================
For general questions regarding the benchmark: `Tuhfe Göçmen <tuhf@dtu.dk>`_

For TotalControl LES dataset - SP-Wind: `Ishaan Sood <ishaan.sood@kuleuven.be>`_ , EllipSys3D: `Søren Juhl Andersen <sjan@dtu.dk>`_

For CL-Windcon LES dataset: `Irene Eguinoa Erdozain <ieguinoa@cener.com>`_ & `Oscar Eduardo Pires Jimenez <oepires@cener.com>`_

For CL-Windcon Wind Tunnel Experiments: `Filippo Campagnolo <filippo.campagnolo@tum.de>`_

For Wind Farm Field Data: `Thomas Duc <thomas.duc@engie.com>`_

