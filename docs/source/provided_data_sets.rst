.. _provided_data_sets:

Provided Data Sets
===================

The FarmConners consortium agreed to build a test benchmark using available data sets from previous and ongoing WFC campaigns. These data sets are collected and released as common test benchmark case for code comparison. The examples of data processing and extraction for every :ref:`test_cases` are provided in the `GitLab repository <https://gitlab.windenergy.dtu.dk/farmconners/farmconners-wind-farm-control-benchmark>`_. The benchmark participants, see :ref:`how_to_participate`, will receive data access links  per test case.  However, if you are interested in the full dataset, feel free to `contact us <tuhf@dtu.dk>`_.

..  figure:: images/DataTexture.png
	:align: center

The benchmark contains data from high-fidelity simulations, wind tunnel measurements, and a full field experiment consisting down-regulation and wake steering operation periods. This variety allows comparison across site-specific dynamics.

The data set is divided into two periods to resemble field application of WFC models. The first period **(calibration)** is to be used to calibrate the control-oriented models for the normal operation data set, where both input and output quantities of interest are provided. In the second period **(blind test)**, the calibrated models are to be run *blindly* and only the input signals are available. In that regard, the blind comparison is expected to show the overall performance of the state-of-the-art WFC models, as well as the common trends and differences among them.  

.. _Sytheticdata:

Synthetic data set - Large Eddy Simulations
----------------------------------------------------
Validation of control-oriented engineering models requires accurate reference data. In addition to the available field measurement data, which could be limited, high-fidelity large-eddy simulation (LES) tools provide a virtual wind-farm environment from which synthetic measurements can be taken under controlled conditions. They are often used to calibrate (or train) and validate the lower fidelity (and computationally lower cost) models.

TotalControl LES:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
..  figure:: images/TC_RWF_turbID.png
	:figwidth: 30%
	:align: right
	
	Layout of the Total Control Reference Wind Power Plant (TC RWP). Axes are distances normalised by the rotor diameter, *i.e.* s/D, where D=178m.
	
One of the core activities within the `TotalControl project <https://www.totalcontrolproject.eu/>`_ is the development and validation of appropriate end-to-end wind-farm simulation models that cover the whole chain from flow model over aero-elastic model to power-grid model. In this regard, a high-fidelity reference database is generated using two independent numerical platforms: SP-Wind by KU Leuven and EllipSys3D by DTU. 

The database is constructed for the TotalControl Reference Wind Power Plant (TC RWP) which consists of 32 turbines in a staggered pattern. The reference turbines are the DTU 10 MW turbines, with a hub height of 119m and a rotor diameter of 178m. The database consists of simulations of different atmospheric conditions and different orientations of the TC RWP. 

CL-Windcon LES:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

..  figure:: images/CL_allWT.png
	:align: left
	
The `CL-Windcon project <http://www.clwindcon.eu/>`_, as one of the major European research projects on wind farm control, has established different pillars for validation of farm models and control strategies. Among them, high-fidelity simulations have played a relevant role. Specifically, a set of SOWFA simulations has been performed and made accessible to the community through a public database. SOWFA, developed by NREL, is an open-source numerical simulation system employed for the high-fidelity simulation of turbulent atmospheric flows together with the analysis of wind plant and wind turbine fluid physics and structural response. The tool builds on top of the OpenFOAM Computational Fluid Dynamics (CFD) tool-kit and NREL's aeroelastic wind turbine simulation tool OpenFAST.
    
Several combinations of wind speeds (3), turbulence intensities (3), and roughness lengths (3), together with yaw misalignment (up to :math:`\pm 30^\circ`), and also de-ratings of wind turbines (in 5% power steps) have been computed at single wind turbine (WT), 3WT and 9WT scenarios. The turbine model is the 10 MW wind turbine developed in the INNWIND.EU project. Within this extensive dataset, the flow case with mean axial inflow 7.7 m/s, turbulence intensity 5.39% and shear height 0.001 m is selected, given the potential benefit reported in the literature. 
    

.. _WindTunnelExp:

CL-Windcon Wind Tunnel Experiments
---------------------------------------------------------------
Data from experiments conducted in `CL-Windcon project <http://www.clwindcon.eu/>`_ within the boundary layer test section of the Politecnico di Milano wind tunnel and with fully actuated and sensorized scaled models is made available for the benchmark exercise. The experimental setup is a scaled wind farm composed of up to three scaled wind turbine models was installed on the 13\,m diameter turntable, which could be rotated to simulate different wind direction.

..  figure:: images/Assembly.png
	:figwidth: 50%
	:align: right
    
The wind turbines used during the experiments are three identical G1 scaled models, whose rotor diameter, hub height and rated rotor speed are 1.1m, 0.825m and 850rpm, respectively. The models are equipped with active pitch, torque and yaw control. They also feature a comprehensive onboard sensorization, including measures of shaft and tower bending moments, and they have been designed with the goal of achieving a realistic energy conversion process and to support the development and testing of wind farm control strategies. Each wind turbine model is also controlled by its own real-time modular Bachmann M1 system, where supervisory control functions, pitch-torque-yaw control algorithms, and all necessary safety, calibration and data logging functions are implemented. 

Within the wind tunnel, two different atmospheric boundary layers (ABLs) were simulated through the use of spires placed at the inlet of the test section. The two ABLs can be considered quite typical of both onshore and offshore sites (with neutral atmospheric stability) and are characterized by vertical wind profiles that are best-fitted by exponential laws with exponent 0.14 and 0.21, respectively for the offshore and onshore ABL. The turbulence intensities at hub height are instead approx. 6% and 13% for the offshore and onshore ABL, respectively.

The available dataset consists of time series of several signals (rotor speed and azimuth, blades pitch, nacelle angle, shaft and tower loads) measured onboard each of the scaled wind turbine within the cluster, as well as time histories of the flow speed measured, by means of three components hot-wire probes, at many locations within a single and multiple wake shed by wind turbines operating at a wide range of conditions. Particularly, the single wake shed by a de-rated and yaw misaligned wind turbine has been traversed along horizontal and vertical lines at several distances downstream. Similarly, the wake shed after the second wind turbine of a 2-machines cluster has been traversed along horizontal lines.

.. _FieldData:

Wind Farm Field Data
--------------------------------------------------------------------

..  figure:: images/SMV_layout.png
	:figwidth: 40%
	:align: right

Data from a commercial wind farm is made available for the benchmark exercise. The wind farm is Sole du Moulin Vieux (SMV), which has been used for the wind farm control field tests of the `SMARTEOLE research project <https://anr.fr/en/funded-projects-and-impact/funded-projects/project/funded/project/b2d9d3668f92a3b9fbbf7866072501ef-c01c47f06c/?tx_anrprojects_funded%5Bcontroller%5D=Funded&cHash=706d15bd225050440ed9d8ba67c901b8>`_.  It is made of 7 wind turbines Senvion MM82 2.05MW on a non-complex terrain, though some local effects can be observed, in particular due to the presence of a forest south of the farm.

The available dataset consists of twenty-nine months of 10-min statistics (average, standard deviation, minimum and maximum values) SCADA data covering the `20 most critical SCADA signals <https://www.windfarmcontrol.info/-/media/Sites/WindfarmControl/Benchmark_ENGIE_SCADA_List.ashx?la=da&hash=A84D25561E4B90595579BDD958AF876861CCAAE0>`_, for each wind turbine in the farm. Twelve months of data with wind farm under normal operation will be used for the calibration of the models, while the remaining dataset (including another twelve months of normal operation and five months during which both wake steering and axial induction tests were led on turbine SMV6) will be used for the benchmark. 10-min statistics data from a ground-based lidar Windcube V2 located closed to turbine SMV6 will also be provided so that the misalignment of the SMV6 wind turbine can be precisely calculated for each 10-min during the wake steering field test and be fed into any wake deflection model. Details on data availability is illustrated below.

..  figure:: images/SMV_field-data.png
	:align: left
	
	Overview of SMV field data provided for the FarmConners benchmark exercise. Availability of both lidar Windcube and wind turbine 10-min SCADA is indicated, along with the different periods used for calibration and blind test. External wind conditions are also shown: wind speed and wind direction are taken from SMV1 SCADA signals while atmospheric stability is derived from MERRA2 data at grid point N50 - E2.5 (the closest from the wind farm). For the wind direction the five sectors are : South wake :math:`175-200^\circ`, South-Western wake :math:`200-225^\circ`, North wake :math:`355-20^\circ`, North-Eastern wake :math:`20-45^\circ` and no wake (other wind directions).

.. _DataSummary:

Summary of Provided Data sets:
-----------------------------------------------------------
	
+----------------+-------------+-------------------+---------------+----------------+
| Data set       | Calibration | Blind test        | Sampling      | Available      |
|                | Period      | Period            | rate          | signals        |
+================+=============+===================+===============+================+
| TotalControl   |             |20min Normal Op.   |               |Full Simulation:|
| LES (SP-Wind,  | 40min       |Additional WFC Op. | >1Hz          |Flow & turbine  |
| EllipSys-3D)   |             |                   |               |signals         |
+----------------+-------------+-------------------+---------------+----------------+
| CL-Windcon     | 600 - 800s  |600 - 800s         | Flow (10s),   |Flow & turbine  |
|  LES (SOWFA)   | cases       |cases              | turbine(0.03s)|signals         |
+----------------+-------------+-------------------+---------------+----------------+
| CL-Windcon Wind| Several     |Several hours      | 250, 2500Hz   | Flow & turbine |
| Tunnel         | hours       |                   |               | signals        |
| experiments    |             |                   |               |                |
+----------------+-------------+-------------------+---------------+----------------+
| Wind Farm      | 12months    |12months Normal Op.| 10min         | SCADA          |
| field data     |             |5months WFC Op.    |               | Windcube lidar |
+----------------+-------------+-------------------+---------------+----------------+
