.. _frequently_asked_questions:

FAQ
==================
Here you can find answers to frequently asked questions. If you need further assistance, feel free to :ref:`contact_us`.

Why should I participate?
	FarmConners WFC benchmark is designed as a consensus of  all the stakeholders in the field and it provides a unique opportunity in assessing the performance of the state-of-the-art WFC-oriented models. It provides a platform to compare your results with your colleagues in the WFC field. Additionally, as a participant it allows you to collaborate further with others in the community and be a part of advancement in the WFC technology.

Is there a deadline for officially joining the benchmark? 
	There is no deadline for registration itself but we do have a deadline for result submission on **15/02/2021 (extended)**. In order to get access to the data, as well as its specific documentation, and start getting familiar with it, we suggest as early registration as possible.

Is there any rule or limitation as to how many test cases or datasets we can choose? Do we need to select a dataset for each test case, or at least one full test case, etc..
	There is no upper or lower limit in the test cases or datasets per test cases you would like to investigate. You can choose as many test cases and as many datasets per test case as you’d like.

Should we end up including the LES or wind tunnel datasets, would we then be required to provide results also for the load alleviation aspect, or could we opt out from this one?
	We expect participants to run their models according to their capabilities. Meaning that if you don’t get the load output from the setup you use for the benchmark, you can opt out for those. In short, we expect at least the wind speed and power output from the models, load components are optional.

Are the submissions and potential publications of the results anonymised?
	Absolutely. We are assigning a participant ID per person and while compiling the results for publication(s), we will likely collect the models under certain tags as well (e.g. there could be several participants using FLORIS). The labels will then look like paticipantXX_modelYY in the results. As a participant, you will know your own participant ID hence you can still see how your model(s) compare to others. We will then get back to all the participants to check if they would prefer not to appear as a co-author in the publication(s), when the results are ready.

For any given dataset, would we be allowed to submit multiple sets of results based on different models, or would we need to choose just one?
	Participants are in fact encouraged to apply more than one model to the BLIND tests. The compiled results would then look like, for example, participantX1_modelY1, participantX1_modelY2, etc. 

Can we run our model based on different control strategies?
	The FarmConners WFC benchmark aims to test the model performance under given WFC strategy, rather than to optimise performance under WFC. Corresponding control inputs are given in the provided datasets. However, there is an AEP optimisation exercise in :ref:`FieldData` where you can implement any control strategy of your choice. Further information on the exercise is included in the documentation forwarded to the participants of :ref:`FieldData` test cases. 


