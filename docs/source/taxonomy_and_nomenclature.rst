.. _taxonomy_and_nomenclature:

General Taxonomy and Nomenclature
====================================

The descriptions for general abbreviations and symbols referred in the benchmark are listed here. The data specific abbreviations, *e.g.* description of the data channels, are attached to the dataset sent to the registered benchmark participants. 

+------------------+--------------------------------------------------------------------+---------------+
| Abbreviation or  | Description, as referred within the benchmark                      | Unit          |
| Symbol           |                                                                    |               |
+==================+====================================================================+===============+
| WFC              |`Wind Farm (flow) Control <https://www.windfarmcontrol.info/about>`_| -             |
+------------------+--------------------------------------------------------------------+---------------+
| TC RWP           |`TotalControl <https://www.totalcontrolproject.eu/>`_               | -             |
|                  |Reference Wind Power Plant                                          |               |
+------------------+--------------------------------------------------------------------+---------------+
| CL-Windcon       |`CL-Windcon <http://www.clwindcon.eu/>`_: EU H2020 project on       | -             |                                       
|                  |Closed Loop Wind Farm Control                                       |               |
+------------------+--------------------------------------------------------------------+---------------+
| SMV              | Sole du Moulin Vieux wind farm                                     | -             |
+------------------+--------------------------------------------------------------------+---------------+
| WT               | Wind turbine                                                       | -             |
+------------------+--------------------------------------------------------------------+---------------+
|:math:`Op.=WFC`   | Operation under WFC                                                | -             |
+------------------+--------------------------------------------------------------------+---------------+
|:math:`Op.=Normal`| Normal (or greedy) operation                                       | -             |
+------------------+--------------------------------------------------------------------+---------------+
| :code:`P_i`      | Power produced by turbine :code:`i` within the wind farm           | kW            |
+------------------+--------------------------------------------------------------------+---------------+
| :code:`U_i`      | Hub-height wind speed at turbine :code:`i` within the wind farm    | m/s           |
+------------------+--------------------------------------------------------------------+---------------+
| :code:`REWS_i`   |Rotor Effective Wind Speed at turbine :code:`i` within the wind farm| m/s           |
+------------------+--------------------------------------------------------------------+---------------+
| :math:`\Delta u` | Non-dimensionalised wake loss                                      | -             |
+------------------+--------------------------------------------------------------------+---------------+
| :math:`TI`       | Turbulence Intensity                                               | -             |
+------------------+--------------------------------------------------------------------+---------------+
| :math:`DEL`      | Damage Equivalent Loads                                            | kNm           |
+------------------+--------------------------------------------------------------------+---------------+
| :code:`FlapM_i`  | Flapwise root bending moment at turbine :code:`i`                  | kNm           |
+------------------+--------------------------------------------------------------------+---------------+
| :code:`ShaftM_i` | Total shaft bending moment at turbine :code:`i`                    | kNm           |
+------------------+--------------------------------------------------------------------+---------------+
| :code:`TBBM_i`   | Total tower bottom bending moment at turbine :code:`i`             | kNm           |
+------------------+--------------------------------------------------------------------+---------------+
| *min*            | minimum of a given variable in a given time period                 | -             |
+------------------+--------------------------------------------------------------------+---------------+
| *max*            | maximum of a given variable in a given time period                 | -             |
+------------------+--------------------------------------------------------------------+---------------+
| *std*            | standard deviation of a given variable over a given time period    | -             |
+------------------+--------------------------------------------------------------------+---------------+

